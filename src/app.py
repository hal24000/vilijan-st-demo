import cvwb
from cvwb.data.repositories import MongoDatasetRepository
from cvwb.data_extraction.json_util import load_json

import pandas as pd
import streamlit as st
import plotly.graph_objects as go
import random

AVAILABLE_TENANTS = ["DURAVERMEER", "BEJO"]


def setup_label_map():
    if "label_map" not in st.session_state:
        label_map = {}
        for tenant in AVAILABLE_TENANTS:
            db = cvwb.data.database.get_database(tenant=tenant)

            label_id_to_attributes = {}
            attribute_id_to_properties = {}

            for attribute in db.attributes.find():
                attribute_id_to_properties[str(attribute["_id"])] = (attribute["name"], attribute["values"])

            for label in db.labels.find():
                label_id = str(label["_id"])
                label_id_to_attributes[label_id] = [attribute_id_to_properties[str(att_id)]
                                                    for att_id in label.get("attribute_ids", [])]

            label_map[tenant] = label_id_to_attributes

        st.session_state.label_map = label_map


def setup_local_label_map():
    label_map = load_json('label_map.json')
    if "label_map" not in st.session_state:
        st.session_state.label_map = label_map


def update_dataset_state():
    if "dataset" in st.session_state:
        interval_dataset = st.session_state.dataset
    else:
        # hardcoded logic.
        if st.session_state.tenant == "BEJO":
            interval_dataset = cvwb.data.Dataset.load('bejo_dataset.json')
        else:
            interval_dataset = cvwb.data.Dataset.load('duravermeer_data.json')

    st.session_state.attributes = set([att.name
                                       for img_src in interval_dataset
                                       for ann in img_src.annotations
                                       for att in ann.base_label.attributes])

    st.session_state.frame_id_to_img_src = {img_src.frame_id: img_src for img_src in interval_dataset}

    data = []

    for img_src in interval_dataset:
        for ann in img_src.annotations:
            curr_row = []  #
            att_dict = {}
            for att in ann.base_label.attributes:
                att_dict[att.name] = att.value
            curr_row.append(img_src.frame_id)
            curr_row.append(ann.base_label.name)
            curr_row.append(ann.base_label.mongo_id)
            for attribute_name in st.session_state.attributes:
                curr_row.append(att_dict.get(attribute_name, ""))

            data.append(curr_row)

    st.session_state.dataset_df = pd.DataFrame(data=data,
                                               columns=["frame_id", "label_name", "label_id"] +
                                                       list(st.session_state.attributes))

    st.session_state.interval_dataset = interval_dataset


def retrieve_job_interval_dataset():
    st.session_state.dataset = MongoDatasetRepository.prepare_job_interval_dataset(tenant=st.session_state.tenant,
                                                                                   dataset_name="",
                                                                                   min_job_id=st.session_state.min_job_id,
                                                                                   max_job_id=st.session_state.max_job_id)


def data_extraction_filters_ui():
    st.sidebar.title("Data extraction")

    st.session_state.tenant = st.sidebar.selectbox("Select the tenant", AVAILABLE_TENANTS)
    st.session_state.min_job_id = st.sidebar.number_input("Select the minimum job number", value=100, step=1)
    st.session_state.max_job_id = st.sidebar.number_input("Select the maximum job number", value=110, step=1)
    _ = st.sidebar.button(label="Fetch the data", on_click=retrieve_job_interval_dataset)

    update_dataset_state()


def attributes_filters_ui():
    dataset_df = st.session_state.dataset_df

    st.sidebar.title("Filter the data")
    st.session_state.prev_label = st.session_state.label if "label" in st.session_state else None
    st.session_state.label = st.sidebar.selectbox("Label", dataset_df.label_name.unique())

    filtered_df = dataset_df[dataset_df.label_name == st.session_state.label].reset_index(drop=True)
    selected_label_id = filtered_df.label_id.values[0]

    if len(filtered_df.label_id.unique()) > 1:
        selected_label_id = st.sidebar.selectbox("Label ID", filtered_df.label_id.unique())

        filtered_df = filtered_df[filtered_df.label_id == selected_label_id].reset_index(drop=True)

    st.session_state.selected_label_id = selected_label_id

    st.sidebar.title("Attribute filters")
    label_attributes = st.session_state.label_map[st.session_state.tenant][selected_label_id]

    selected_attribute_values = [[] for _ in range(len(label_attributes))]

    for i, (attribute_name, attribute_values) in enumerate(label_attributes):
        selected_attribute_values[i] = st.sidebar.multiselect(f'Filter by: {attribute_name}',
                                                              attribute_values,
                                                              [])

    for i, (attribute_name, attribute_values) in enumerate(label_attributes):
        if len(selected_attribute_values[i]) > 0:
            filtered_df = filtered_df[filtered_df[attribute_name].isin(selected_attribute_values[i])].reset_index(
                drop=True)

    st.session_state.filtered_dataset_df = filtered_df


def overall_stats_ui():
    interval_dataset = st.session_state.interval_dataset
    dataset_df = st.session_state.dataset_df

    no_of_annotated_images = len([img_src for img_src in interval_dataset if img_src.has_annotations])
    no_of_unannotated_images = len(interval_dataset) - no_of_annotated_images
    annotated_percent = round((no_of_annotated_images / len(interval_dataset)) * 100, 2)
    no_of_annotations = len([ann for img_src in interval_dataset for ann in img_src.annotations])

    st.subheader("General statistics of the interval dataset")

    with st.expander("Statistics overview", expanded=True):

        stat_cols = st.columns(4)

        stat_cols[0].text("Images")
        stat_cols[0].success(len(interval_dataset))

        stat_cols[1].text("Annotated images")
        stat_cols[1].success(no_of_annotated_images)

        stat_cols[2].text("Unannotated images")
        stat_cols[2].success(no_of_unannotated_images)

        stat_cols[3].text("Annotations")
        stat_cols[3].success(no_of_annotations)

        if annotated_percent > 80:
            st.success(f"{annotated_percent}% of the images are annotated.")
        else:
            st.error(f"{annotated_percent}% of the images are annotated.")

        label_distribution = dataset_df.label_name.value_counts()
        fig = go.Figure([go.Bar(x=list(label_distribution.keys()),
                                y=label_distribution.values)])

        fig.update_layout(
            title="Label distribution",
            title_x=0.5,
            xaxis_title="Label name",
            yaxis_title="Number of annotations",
        )

        st.plotly_chart(fig, use_container_width=True)


def custom_statistics_ui():
    filtered_dataset_df = st.session_state.filtered_dataset_df
    st.subheader("Statistics of interest")
    description = """
        - Use the custom filters from the sidebar in order to retrieve the stats that you want
        - Filter by all the possible attributes
    """
    st.write(description)

    st.text("Sample images")
    st.error("Not supported currently.")
#     sample_cols = st.columns(5)
#     for i in range(5):
#         frame_id = random.choice(filtered_dataset_df.frame_id.values)
#         image_source = st.session_state.frame_id_to_img_src[frame_id]
#         cv_image = cvwb.data.storage.get_cv_image(image_source=image_source,
#                                                   tenant=st.session_state.tenant)
#         cv_image.image_source.annotations = [ann for ann in cv_image.image_source.annotations
#                                              if ann.base_label.name == st.session_state.label]
#         sample_cols[i].image(image=cv_image.annotated_image(),
#                              caption=cv_image.image_source.frame_id,
#                              use_column_width=True)

    info_cols = st.columns(3)

    info_cols[0].text("Selected label")
    info_cols[0].success(st.session_state.label)

    info_cols[1].text("Annotated Images")
    info_cols[1].success(len(filtered_dataset_df.frame_id.unique()))

    info_cols[2].text("Annotations")
    info_cols[2].success(len(filtered_dataset_df))

    label_attributes = st.session_state.label_map[st.session_state.tenant][st.session_state.selected_label_id]
    attribute_names = [att_name for att_name, _ in label_attributes]

    target_distribution = st.selectbox("Select distribution attribute", attribute_names)
    distribution = filtered_dataset_df[target_distribution].value_counts()

    x_axis = []
    y_axis = []

    for k, v in distribution.items():
        x_axis.append(k)
        y_axis.append(v)

    fig = go.Figure([go.Bar(x=x_axis, y=y_axis)])

    fig.update_layout(
        title=f"{target_distribution} distribution",
        title_x=0.5,
        xaxis_title=f"{target_distribution} values",
        yaxis_title="Number of annotations"
    )

    st.plotly_chart(fig, use_container_width=True)


# setup_label_map()

setup_local_label_map()

data_extraction_filters_ui()

if "interval_dataset" not in st.session_state:
    update_dataset_state()

overall_stats_ui()

attributes_filters_ui()

custom_statistics_ui()
